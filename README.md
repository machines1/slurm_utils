# SLURM UTILS

A simple script for automating the submission of multiple jobs in slurm.
Create a file `<cmd.sh>` containing one command per line.

Run `parallel_srun -f <cmd.sh> -m <mem> -c <cpus> -t <time>` and each command will be added to the queue.
This is useful for automating submission while keeping a copy of each command that has been submitted.