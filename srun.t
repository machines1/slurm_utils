#!/bin/bash -l

#SBATCH --nodes @NODES
#SBATCH --ntasks-per-node @TASKS_PER_NODE

## cpus-per-task > 1 needed for multithreaded applications
#SBATCH --cpus-per-task @CPUS_PER_TASK

## maximum memory needed in MB
#SBATCH --mem @MEMORY

## maximum walltime needed
#SBATCH --time @TIME

@INITIALIZE

srun @COMMAND
